const express = require("express");
const app = express();
const errorMiddleware = require("./middleware/error");
const cinema = require("./routes/cinemaHallRoute");
const user = require("./routes/userRoute");
const cookieParser = require("cookie-parser")

app.use(express.json());
app.use(cookieParser());


//Routes
app.use("/api/v1", cinema)
app.use("/api/v1", user)

// Middleware for Error Handling
app.use(errorMiddleware);

module.exports = app;
