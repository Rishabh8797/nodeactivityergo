const Cinema = require("../models/cinemaHallModel");
const ErrorHandler = require("../utils/errorHandler");
const catchAsyncError = require("../middleware/catchAsyncError");
const ApiFeatures = require("../utils/apiFeatures");

//Create new CinemaHall
exports.createCinema = catchAsyncError(async (req, res, next) => {
	const cinema = await Cinema.create(req.body);
	res.status(201).json({ success: true, cinema })
})


//Get All cinema Hall List with Pagination, search using name and filter using state
exports.getAllCinemaHall = catchAsyncError(async (req, res) => {

	const cinemaCount = await Cinema.countDocuments()
	const apiFeatures = new ApiFeatures(Cinema.find(), req.query).filter()

	const cinemas = await apiFeatures.query;
	console.log(cinemas);
	res.status(200).json({
		success: true,
		cinemas,
		cinemaCount
	})
})


//Get Cinema Details
exports.getCinemaDetails = catchAsyncError(async (req, res, next) => {
	const cinema = await Cinema.findById(req.params.id);
	if (!cinema) {
		return next(new ErrorHandler("Cinema not Found", 404));
	}
	res.status(200).json({
		success: true,
		cinema
	})
})

// Delete Cinema

exports.deleteCinema = catchAsyncError(async (req, res, next) => {
	const cinema = await Cinema.findById(req.params.id);
	if (!cinema) {
		return next(new ErrorHandler("Cinema not found", 404))
	}
	await cinema.remove();
	res.status(200).json({
		success: true,
		message: "Cinema deleted successfully"
	})

})

//Update Cinema Hall Details

exports.updateCinema = catchAsyncError(async (req, res, next) => {
	let cinema = await Cinema.findById(req.params.id);
	if (!cinema) {
		return next(new ErrorHandler("Cinema not found", 404))
	}

	cinema = await Cinema.findByIdAndUpdate(req.params.id, req.body, {
		new: true,
		runValidators: true,
		useFindAndModify: false
	});

	res.status(200).json({
		success: true,
		cinema
	})

})
