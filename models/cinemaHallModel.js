const mongoose = require('mongoose');

const cinemaSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Please enter cinemaHall name"]
	},
	state: {
		type: String,
		required: [true, "Please enter a state"]
	},
	movies: [
		{
			movieName: {
				type: String,
				required: [true, "Please enter a movie name"]
			},
			movieTimings: [
				{
					type: String,
					required: [true, "please enter movie timings"]
				}
			]
		}
	],
	createdAt: {
		type: Date,
		default: Date.now
	}
})


module.exports = mongoose.model("Cinema", cinemaSchema);
