const express = require('express');
const { createCinema, getAllCinemaHall, getCinemaDetails } = require('../controllers/cinemaController');
const { isAuthenticatedUser } = require("../middleware/auth");
const router = express.Router();

router.route("/cinema/new").post(createCinema)
router.route("/cinemas").get(isAuthenticatedUser, getAllCinemaHall)
router.route("/cinema/:id").get(getCinemaDetails)


module.exports = router;
