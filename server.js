const app = require('./app');
const dotenv = require('dotenv');
const connectDatabase = require("./config/database");

//Handling Uncaught Exception
process.on("uncaughtException", (err) => {
	console.log(`Error: ${err.message}`);
	console.log("Shutting down server due to unCaught exception");
	process.exit();
})

//Config
dotenv.config({ path: "config/config.env" });


//Connecting to database
connectDatabase();


const server = app.listen(process.env.PORT, () => {
	console.log(`Server is running at port ${process.env.PORT}`);
})


//Unhandled Promise rejection
process.on("unhandledRejection", err => {
	console.log(`Error: ${err.message}`);
	console.log("Shutting down the server due to unhandled promise rejection");
	server.close(() => {
		process.exit(1);

	});
})
