class ApiFeatures {
	constructor(query, queryStr) {
		this.query = query
		this.queryStr = queryStr
	}


	filter() {
		const queryCopy = { ...this.queryStr };
		const removeFields = ["name"];
		removeFields.forEach((key) => delete queryCopy[key]);
		this.query = this.query.find(queryCopy);
		return this;

	}


}

module.exports = ApiFeatures;
